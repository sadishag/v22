// JavaScript Document

// Initial Settings
level = 1;
theme = "";
langue = "";
lesTrads="";

// Lecture de la langue du mobile
/*navigator.globalization.getLocaleName(
    function (locale) {alert('locale: ' + locale.value + '\n');langue=locale.value.substring(0,2);},
    function () {alert('Error getting locale\n');}
);*/

	
$(document).ready(function(){
	
	//Récupération des réglages
	loadSettings();
	
	document.addEventListener("backbutton", BackButtonPressed, false);
     
	function BackButtonPressed() {
	   window.navigate("index.html");
	}
	
	// Les traductions sont enregistrées en variable de cession pour éviter d'occuper la mémoire lorsque l'appli n'est pas utilisée. Et pouvoir utiliser la langue dans toutes les pages (contratirement à un evariable 
	$.get("traductions.xml",{},function(xml){
		var xmlSer = new XMLSerializer().serializeToString(xml);
		sessionStorage.setItem("lesTrads",xmlSer); // Pour utiliser dans les autres pages
	});
	
	if(localStorage.getItem('langue') == null) {
		localStorage.setItem("langue","en");
		localStorage.setItem('niveau',"1");
		localStorage.setItem('theme',"couleurs");
	}
	
	
	// On désactive les transition pour android car il ne sait pas bien les gérer
	var usera = navigator.userAgent.toLowerCase();
	var isAndroid = usera.indexOf("android") > -1;
	if(isAndroid) {
	//	$.mobile.maxTransitionWidth = 1;
		$.mobile.defaultPageTransition = 'none';
	}
	
	// Pour permettre la mise à jour de la page settings
	
	// Pour locker l'orientation
//AndroidManifest.xml   : <activity   ........      android:screenOrientation="portrait">
//PhoneGap.plist  : 
/*<dict>
`.....
    <array>
        <string>UIInterfaceOrientationPortrait</string>
        <string>UIInterfaceOrientationPortraitUpsideDown</string>
    </array>
..........
</dict>*/


//$('#radio').bind('change',function(){ alert($(this));});

// On mesure la taille de la page au bout d'un certain délai
timerInit = 0;

	// Chargement des traductions
	//chargeLangues();
	
	traduitPage();
});
								 


function traduitPage(){	
	// Traduction des textes de la page
	$("#aGame").text(texte(2)); 
	$('#btPlay .ui-btn-text:eq('+0+')').text(texte(3));
	// Barre de nav pages 1
	$('#navP1 .ui-btn-text:eq('+0+')').text(texte(4));
	$('#navP1 .ui-btn-text:eq('+1+')').text(texte(5));
	$('#navP1 .ui-btn-text:eq('+2+')').text(texte(6));
	$('#navP1 .ui-btn-text:eq('+3+')').text(texte(7));
	$('#navP1 .ui-btn-text:eq('+4+')').text(texte(8));
}

function quitter(){
	navigator.app.exitApp();
}



// Prise en compte des réglages
function loadSettings(){
	if(typeof localStorage!='undefined') {
		if(localStorage.getItem('langue')!=null) {
			langue = localStorage.getItem('langue');
			level = localStorage.getItem('niveau');
			theme = localStorage.getItem('theme');
		}
	}else{
		alert("Attention localStorage n'est pas supporté");		
	}
}

function texte(txtNum){
	leTxt = $(sessionStorage.getItem("lesTrads")).find('unTexte[num='+txtNum+']').find(langue).text();
	return(leTxt);
}

function regles(){
	window.location.href="rules-"+localStorage.getItem('langue')+".html";
}

