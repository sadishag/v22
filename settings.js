/**
 * ...
 * @author ...
 */
	

// Enregistrement des réglages depuis la page de settings
function majSettings(){

	// Niveau
/*	if($("#level_0").is(':checked')){
		level = 0;
	}else if($("#level_1").is(':checked')){
		level = 1;
	}else if($("#level_2").is(':checked')){
		level = 2;
	}else{
		level = 3;
	}*/
	//level = $('#choixNiveau input[type=radio]:checked').attr("value")

	//$("#lesLevels").prop("checked",true).checkboxradio("refresh");

	
	if(typeof localStorage!='undefined') {
		
		// Comptage du nombre d'utilisations pour la version d'évaluation
		var nbvisites = localStorage.getItem('visites');
		// Vérification de la présence du compteur
		if(nbvisites!=null) {
			// Si oui, on convertit en nombre entier la chaîne de texte qui fut stockée
			nbvisites = parseInt(nbvisites);
		} else {
			nbvisites = 1;
		}
		// Incrémentation
		nbvisites++;
		
		// Stockage à nouveau en attendant la prochaine visite...
		localStorage.setItem('visites',nbvisites);
		
		// Enregistrement des données d'options
		localStorage.setItem('langue',$('#choixLanguage input[type=radio]:checked').attr("value"));
		localStorage.setItem('niveau',$('#lesLevels').val());
		localStorage.setItem('theme',$('#choixTheme').val());
		
	}	else{
		alert("Attention localStorage n'est pas supporté");	
	}
			
	// Affichage de la page de jeu
	window.location.href="index.html";

}

function traduitPage(){	
				
	// Traduction des textes de la page	
	$('#l1 .ui-btn-text').text(texte(13));
	$('#l2 .ui-btn-text').text(texte(14));
	$('#l3 .ui-btn-text').text(texte(15));
	$('#l4 .ui-btn-text').text(texte(16));
	// Titre des listes
	$('#lesLevels legend:eq('+0+')').text(texte(24));
	$('#lesLevels legend:eq('+1+')').text(texte(23));
	$('#lesLevels legend:eq('+2+')').text(texte(22));
	// Otion des thèmes
	$('#choixTheme option:eq('+0+')').text(texte(17));
	$('#choixTheme option:eq('+1+')').text(texte(18));
	$('#choixTheme option:eq('+2+')').text(texte(19));
	$('#choixTheme option:eq('+3+')').text(texte(20));
	$('#choixTheme option:eq('+4+')').text(texte(21));
	
	$('#txtFree').text(texte(42));
	
	// pied
	$('#navSettings .ui-btn-text:eq('+0+')').text(texte(26));
	$('#navSettings .ui-btn-text:eq('+1+')').text(texte(25));

}