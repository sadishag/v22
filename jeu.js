// JavaScript Document
lesCouleurs = Array(["white","#ffffff"], ["blue","#0101ff"], ["green","#01ff15"], ["yellow","#fef900"], ["red","#c00000"], ["orange","#ffaf02"], ["purple","#b211d9"], ["sky", "#02e3ff"], ["black","#000000"]);

cubesActifs = false;
laCouleurTiree = 0;
laCouleurChoisie = 0;
dureeInit = 60;
duree = dureeInit;
gagne = false;
nbCoupsMax = 12;
nbCoups = nbCoupsMax;
termine = false;
timerTirCoul = 0;
timerLanceMin = 0;
timerRetourne = 0;
timerMessage = 0;
dureePile = 2000;
phase = "mémo";
deplaceCube = false;
theCube2move = "0,0";
lesTrads = "";
reset = false;
langue = "en";
nbEvaluations = 300;
								 


function play(){
	clearTimeout(timerLanceMin);
	timerLanceMin = 0;
	
	nbCoupsMax = 12;
	nbCoups = nbCoupsMax;
	
	resetPions();
	
//	phase = "play";
	message(" ",$('#minuterie'),0);
	message(" ", $('#msgBas'), 0);
	
	// Les couleurs disparaissent
	$('.unCube').css({backgroundColor: '#ffffff', 'background-image':'url(images/pileCubes.png)', 'background-repeat':'no-repeat',  'background-position':'center center'});

	termine = false;
	message(texte(29), $('#msgSeul'), 0);//"You have 12 moves to give 6 correct answers."

	tirageCouleur();
			
	// L'action du bouton change
	$('#btRet').unbind('click');
	$('#btRet').bind('click',function(){setup();});
}

function installHaut(){
	ajout = "";
	ajout += "<div class=ui-block-a id='minuterie' width='20px'></div>";
	var leNivTxt = texte(13+parseInt(level));
	ajout += "<div class=ui-block-b id='leNiveau' width='100%'>"+leNivTxt.substring(0,leNivTxt.indexOf(":"))+"</div>";
	ajout += "<div class=ui-block-c id='coups' width='20px'></div>";
	
	$('#barreHaut').append(ajout);	
}

function installCubes(){
	// Vérification du nombre de parties
	var nbParties = localStorage.getItem('nbParties');
	if(nbParties>nbEvaluations){
		// L'évaluation est terminée
		window.location.href="index.html";
	}else{
		// L'évaluation n'est pas terminée, on lance une autre partie
		if(nbParties!=null) {
			// Si oui, on convertit en nombre entier la chaîne de texte qui fut stockée
			nbParties = parseInt(nbParties);
		} else {
			nbParties = 1;
		}
		// Incrémentation
		nbParties++;
		// Stockage à nouveau en attendant la prochaine visite...
		localStorage.setItem('nbParties',nbParties);
		
		// Installation des cubes
		colonne = 1;
		ajout = "";
		var divUnCube = "<p class='unCube' onClick='retourne(this);'></p>";
		for(i=1;i<=3;i++){
			
			// Emplacements des cubes
			ajout += "<div class=ui-block-a>"+divUnCube+"</div>";
			ajout += "<div class=ui-block-b>"+divUnCube+"</div>";
			ajout += "<div class=ui-block-c>"+divUnCube+"</div>";
			
			if(i<3){
				// Pour maintenir un espace vertical entre les cubes
				ajout += "<div class=ui-block-a>&nbsp;</div>";
				ajout += "<div class=ui-block-b>&nbsp;</div>";
				ajout += "<div class=ui-block-c>&nbsp;</div>";
			}
		}
		$('#lesCubes').append(ajout);
			
		$('.unCube').css({width:'90%'});
		$('.unCube').css({height:EcranWidth*0.9});

		// Tirage de couleurs
		tiragePlaces();
	}
}

function tiragePlaces()
{
			
	// Tirage des positions des différentes couleurs pour les cubes
	var ordreCouleurs = new Array();
	while(ordreCouleurs.length < 9){
		nombre = Math.floor(Math.random() * 9);
		while(ordreCouleurs.indexOf(nombre)!= -1){
			nombre = Math.floor(Math.random() * 9);
		}
		ordreCouleurs.push(nombre);
	}
	
	lesCubes = $('.unCube');
	rayon = 30;//$('p.unCube:eq('+(0)+')').width()/2;

	for(i = 0; i<lesCubes.length; i++){	
		// Nommage des id des cubes en place en fonction de la couleur qui leur est affectée	
		lesCubes[i].id = i + "/" + ordreCouleurs[i]; // Position du cube / Sa couleur		
		// Affichage de la couleur	
		$('p.unCube:eq('+i+')').css({'background-image':'url(images/'+lesCouleurs[ordreCouleurs[i]][0]+'.jpg)', 'background-repeat':'no-repeat',  'background-position':'center center', 'border-radius': rayon})

	}
	
	diamCube = 40;	

}

function installLesPions(){
	for(i=0; i<6; i++){
		$('#lesPions').append("<td id='libre' class='unPion' width='10'></td>");
	}
}


function majCompteur(){
	message(nbCoups + ' / ' + nbCoupsMax,$('#coups'),0);
	if(nbCoups > 0){
	}else{
		partieTerminee();
		message(texte(30), $('#msgSeul'), 0);	//"It's a shame. You lost the game!"
	}
}


function message(txt, champ, dureeVie){
	// On affiche le message pendant 3 scondes
	//while(msgEnCours){}; // Attends que le message déjà affiché disparaisse avant d'afficher le nouveau
		
	if(txt==""){
		//$('#msgSeul').css({display:none});		
	}else{
		//$('#msgSeul').css.(border-color:#777});		
	}
	champ.text(txt);
	//clearTimeout(timerMessage);
	timerMessage = 0;

	if(!timerMessage && dureeVie>0){
		timerMessage = setTimeout(function(){
			champ.text('');
		//	$('#msgSeul').css({display:none});	
			timerMessage = 0;
		}, dureeVie*1000);
	}

}

// On retourne un cube
function retourne(theCube) 
{
	if(cubesActifs){ // Pour inhiber les cubes, il suffit de mettre cubesActifs sur false
		
		// On enlève la question
		$('.leDe').css({background:'none'});		
		laCouleurChoisie = theCube.id.split("/");
		
		// Affiche la couleur du cube
		theCube.style.backgroundImage = "url('images/"+lesCouleurs[laCouleurChoisie[1]][0]+".jpg')";
		
		// Inhibe les cubes pour qu'on ne puisse pas cliquer avant qu'il ne soit retourné
		cubesActifs = false;
		
		if(!deplaceCube){
			theCube2move = laCouleurChoisie;
			// Vérifie la réponse
			if(laCouleurChoisie[1] == laCouleurTiree){ 
				// C'est le bon cube
				majPions(lesCouleurs[laCouleurTiree][1], true); // Place le pion gagné
			}else{
				majPions(laCouleurChoisie[0], false);
			}
			majCompteur();
		}else{
			
			if (laCouleurChoisie[1]==laCouleurTiree){
				message(texte(31), $('#msgSeul'), 3);	//"Please, choose a different place."
			}else{
				// Intervesion des 2 cubes				
				interversion(theCube2move,theCube);
			}
		}
	}    
}

function interversion(initialPlace,newPlace){
	newPlace.id = newPlace.id.split("/")[0] + "/" + initialPlace[1];
	newPlace.style.backgroundImage = "url('images/"+lesCouleurs[initialPlace[1]][0]+".jpg')";
	
	$('.unCube')[initialPlace[0]].id = initialPlace[0] + "/" + laCouleurChoisie[1];
	timerRetourne = 0;
//	alert(initialPlace[0]+"//"+newPlace.id.split("/")[0]);
	
	if(level == 3){// On ne montre pas la couleur du nouvel emplacement
		$('p.unCube:eq('+initialPlace[0]+')').css({backgroundColor: '#ffffff', 'background-image':'url(images/pileCubes.png)', 'background-repeat':'no-repeat',  'background-position':'center center'});
		if(!timerRetourne){
			timerRetourne = setTimeout(function(){
				if(deplaceCube){
					$('p.unCube:eq('+laCouleurChoisie[0]+')').css({backgroundColor: '#ffffff', 'background-image':'url(images/pileCubes.png)', 'background-repeat':'no-repeat',  'background-position':'center center'});
					message("", $('#msgSeul'), 0);	
					deplaceCube = false;
					tirageCouleur();
				}
				if(!termine){
					cubesActifs = true;
				}
				timerRetourne = 0;
			}, 2000);
		}

	}else{
		//alert("ll");
		// Cube gagné
	//	$('p.unCube:eq('+laCouleurChoisie[0]+')').css({'background-image':'url(images/'+lesCouleurs[laCouleurChoisie[1]][0]+'.jpg)', 'background-repeat':'no-repeat',  'background-position':'center center', 'border-radius': rayon});
		$('p.unCube:eq('+initialPlace[0]+')').css({'background-image':'url(images/'+lesCouleurs[laCouleurChoisie[1]][0]+'.jpg)', 'background-repeat':'no-repeat',  'background-position':'center center', 'border-radius': rayon});
		//$('p.unCube:eq('+laCouleurChoisie[0]+')').css({backgroundColor: '#ffffff', 'background-image':'url(images/pileCubes.png)', 'background-repeat':'no-repeat',  'background-position':'center center'});
	
		// On retourne le cube du côté caché après 2 secondes
		
		if(!timerRetourne){
			timerRetourne = setTimeout(function(){
				$('p.unCube:eq('+initialPlace[0]+')').css({backgroundColor: '#ffffff', 'background-image':'url(images/pileCubes.png)', 'background-repeat':'no-repeat',  'background-position':'center center'});
				if(deplaceCube){
					$('p.unCube:eq('+laCouleurChoisie[0]+')').css({backgroundColor: '#ffffff', 'background-image':'url(images/pileCubes.png)', 'background-repeat':'no-repeat',  'background-position':'center center'});
					message("", $('#msgSeul'), 0);	
					deplaceCube = false;
					tirageCouleur();
				}
				if(!termine){
					cubesActifs = true;
				}
				timerRetourne = 0;
			}, 2000);
		}
	
	}
}

// Pour convertir les couleurs hexa/rvb pour pouvoir les comparer
function rvb2hex(val){
  if (val.indexOf("rgb") >= 0)  {
	  var rgb_string = val.slice(val.indexOf('(') + 1,val.indexOf(')'));
	  var rgb_val = rgb_string.split(",");
	  val = "#";
	  var hexChars = "0123456789ABCDEF";
	  for (var i = 0; i < 3; i++)  {
		  var v = rgb_val[i].valueOf();
		  val += hexChars.charAt(v/16) + hexChars.charAt(v%16);
	  }
  }
return val;
}

function resetPions(){
	$('.unPion[id^="pris"]').each(function(){
		// On enlève le pion
		this.style.backgroundColor = "#333";
		this.style.border = "1px solid #000";
		this.id = 'libre';										   
	});
}

function majPions(couleur, gagnant){
	
	couleurExiste = false;
	// Recherche si un pion de la couleur existe déjà
	$('.unPion[id^="pris"]').each(function(){
		if(rvb2hex(this.style.backgroundColor).toUpperCase() ==  lesCouleurs[laCouleurTiree][1].toUpperCase()){
			// La couleur a déjà été tirée
			couleurExiste = true;
			if(gagnant){ // Le joueur a trouvé la couleur
				message(texte(32) + lesCouleurs[laCouleurTiree][0] + texte(33), $('#msgSeul'), 3);//"Phew, you keep the "" pawn!"	
				nbCoups++;// On enlève pas de coup dans ce cas -> on ++ puisqu'il va etre -- ça reviens à 0
				// On tire la couleur suivante
			}else{
				message(texte(34) + lesCouleurs[laCouleurTiree][0] + texte(33), $('#msgSeul'), 3);//"No, you lost the "" pawn!"	
				// On enlève le pion
				this.style.backgroundColor = "#333";
				this.style.border = "1px solid #000";
				this.id = 'libre';
				// On remet ce cube sur le cote pileCubes
				timerRetourne1 = 0;				
				if(!timerRetourne1){
					timerRetourne1 = setTimeout(function(){
						$('p.unCube:eq('+couleur+')').css({backgroundColor: '#ffffff', 'background-image':'url(images/pileCubes.png)', 'background-repeat':'no-repeat',  'background-position':'center center'});
						timerRetourne1 = 0;
					}, 1000);
				}						
			}
		}
	});
	
	// La couleur est tirée pour la première foi et trouvée par le joueur
	if(!couleurExiste){
		if(gagnant){
			// On color le premier pion vide
			$('#libre')[0].style.backgroundColor = couleur;
			$('#libre')[0].style.border = "1px solid #AAA";
			$('#libre')[0].id = 'pris';			
		}else{
			message(texte(35), $('#msgSeul'), 2);//"No!"
			//navigator.notification.vibrate(500);
			// On remet ce cube sur le cote pileCubes
			timerRetourne1 = 0;				
			if(!timerRetourne1){
				timerRetourne1 = setTimeout(function(){
					$('p.unCube:eq('+couleur+')').css({backgroundColor: '#ffffff', 'background-image':'url(images/pileCubes.png)', 'background-repeat':'no-repeat',  'background-position':'center center'});
					timerRetourne1 = 0;
				}, 1000);
			}
		}
	}
	
	nbCoups--;
	
	
	if($('.unPion[id^="pris"]').length == 6){ // Le joueur a gagné la partie 6
		//leScore = (nbCoupsMax-nbCoups) + " on " + nbCoupsMax;	
		phase = "go";
		$('#btRet').unbind('click');
		$('#btRet').bind('click',function(){play();});
		message(" ",$('#minuterie'),0);
		message(texte(40), $('#msgSeul'), 0);
		
		// On retourne le pion
		$('p.unCube:eq('+couleur+')').css({backgroundColor: '#ffffff', 'background-image':'url(images/pileCubes.png)', 'background-repeat':'no-repeat',  'background-position':'center center'});
	}else{
		if(gagnant){
			switch(level){
				case '0': // on n'a pas à déplacer le cube
					message(texte(36), $('#msgSeul'), 0);//"Well done!"
					deplaceCube = false;
					timerRetourne1 = 0;				
					if(!timerRetourne1){
						timerRetourne1 = setTimeout(function(){
							$('p.unCube:eq('+laCouleurChoisie[0]+')').css({backgroundColor: '#ffffff', 'background-image':'url(images/pileCubes.png)', 'background-repeat':'no-repeat',  'background-position':'center center'});
						//	message(laCouleurChoisie[0], $('#msgSeul'), 0);
						timerRetourne1 = 0;
						}, 1000);
					}
					break;
				case '1': // Le joueur doit déplacer le cube trouvé
					message(texte(37), $('#msgSeul'), 0);//"Well done! Please, find an other place."			
					// Le joueur doit déplacer le pion
					deplaceCube = true;
					cubesActifs = true;							
					break;
				case '2': // C'est l'appli qui choisi ne nouvel emplacement, la couleur de l'emplacement choisi est révélée
					message(texte(36), $('#msgSeul'), 0);//"Well done!"			
					deplaceCube = true;
					choixNewPlace();
					interversion(theCube2move, theCube);
					break;
				case '3': // C'est l'appli qui choisi le nouvel emplacement, mais la couleur ne s'affiche pas
					choixNewPlace();
					deplaceCube = true;
					interversion(theCube2move, theCube);
					break;					
			}
			
		}
		if(!deplaceCube){
			tirageCouleur();
		}
	}
}

// Choix automatique du nouvel emplacement du cube trouvé
function choixNewPlace(){
	do{
		laNewPlace = Math.floor(Math.random() * 9);
	} while(laCouleurTiree == laNewPlace); 
	
	timerNewPlace = 0;
	
	if(!timerNewPlace){
		timerNewPlace = setTimeout(function(){
			cubesActifs = true; // Pour que la fonction retourne fonctionne
			retourne($('.unCube')[laNewPlace]);
			timerNewPlace = 0;
		}, 2000);
	}

}


function partieTerminee(){	
	stopMinut();
	clearTimeout(timerMessage);
	timerMessage = 0;
	clearTimeout(timerRetourne1);
	timerRetourne1 = 0;
	clearTimeout(timerRetourne);
	timerRetourne = 0;
	clearTimeout(timerTirCoul);
	timerTirCoul = 0;
	clearTimeout(timerLanceMin);
	timerLanceMin = 0;
	
	cubesActifs = false;
	laCouleurTiree = 0;
	laCouleurChoisie = 0;
	dureeInit = 60;
	duree = dureeInit;
	gagne = false;
	/*nbCoupsMax = 12;
	nbCoups = nbCoupsMax;*/
	
	termine = true;	
	$('.leDe').css({background:'none'});
}


function stopMinut(){
	cubesActifs = false; // inhibe les dés le temps que la nouvelle couleur soit choisie
//	gagne = true; // Stop le décompte
	
	
}

function tirageCouleur(){	
	timerTirCoul = 0;	
	if((!timerTirCoul && duree > 0 && !termine) || phase=="go"){
		phase = "play";
		timerTirCoul = setTimeout(function(){
			gagne = false;
			laCouleurTiree = Math.floor(Math.random() * 9);

			$('.leDe').css({background: lesCouleurs[laCouleurTiree][1]});
			
			message(texte(38), $('#msgBas'), 3);//"Find this color!"
			cubesActifs = true;
			timerTirCoul = 0; 
			duree = dureeInit; // On remet la minuterie à zéro			
		}, dureePile);
	}
}

function lanceMinut(){
	timerLanceMin = 0;	
		if(!timerLanceMin && duree > 0 && phase == "mémo"){
			timerLanceMin = setTimeout(function(){
				duree--;
				message(texte(28) + ": "+duree,$('#minuterie'),0);//"Time : "
				//cubesActifs = true;
				timerLanceMin = 0;
			//	if(!gagne && !termine){
					lanceMinut();
				//}
			}, 1000);
		}else{
			// Le temps de mémorisation est dépassé
				//majCompteur();
				message(texte(39), $('#msgSeul'), 3);//"It's time to play now!"
				phase = "go";
				if(!timerLanceMin){
					timerLanceMin = setTimeout(function(){
						//cubesActifs = true;
						timerLanceMin = 0;
						play();
				}, 3000);
				}
				
		}
}

function loadlesSettings(){
	
	if(typeof localStorage!='undefined') {
		if(localStorage.getItem('langue')!=null) {
			langue = localStorage.getItem('langue');
			level = localStorage.getItem('niveau');
			theme = localStorage.getItem('theme');
			
			var xmlOut = sessionStorage.getItem("lesTrads");
			lesTrads = new DOMParser().parseFromString(xmlOut, "text/xml");
			
			$('leNiveau').text('lklklk');
		}
	}else{
		alert("Attention localStorage n'est pas supporté");		
	}
}

